import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

const firebaseConfig = {
	apiKey: "AIzaSyB1xeOrYHymPGUBcZeDW4GjYAh_GytovsQ",
  authDomain: "next-firebase-ceaa2.firebaseapp.com",
  projectId: "next-firebase-ceaa2",
  storageBucket: "next-firebase-ceaa2.appspot.com",
  messagingSenderId: "1021950446163",
  appId: "1:1021950446163:web:a938aa637bb209b64554a6",
  measurementId: "G-QDX8YV777P"
}
if(!firebase.getApps.length){
	firebase.initializeApp(firebaseConfig)
}
export const auth = firebase.auth()
export const firestore = firebase.firestore()
export const storage = firebase.storage()